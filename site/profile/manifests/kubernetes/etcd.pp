# Profile class for configuring etcd on controller nodes
class profile::kubernetes::etcd {
  # Installing etcd and making itself available
  class { 'etcd':
    ensure             => 'latest',
    listen_client_urls => 'http://0.0.0.0:2379',
  }
}
