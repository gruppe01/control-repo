# Profile class for Kubernetes worker nodes
class profile::kubernetes::node {
  # Installing Kubernetes and making it a worker node
  class { '::kubernetes':
    worker => true,
  }
}
