# Profile class for Kubernetes controller nodes
class profile::kubernetes::controller {
  # Installing kubernetes and making it a controller
  class { '::kubernetes':
    controller => true,
  }

}
