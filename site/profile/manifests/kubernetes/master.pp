# Profile class for Kubernetes master nodes
class profile::kubernetes::master {
  # Installing kubernetes, making it controller and bootstrapping the cluster
  class { '::kubernetes':
    controller           => true,
    bootstrap_controller => true,
  }
}
