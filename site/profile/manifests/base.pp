# Base profile class for all nodes
class profile::base {
  # Installing ntp and configuring servers
  class { 'ntp':
    servers => lookup('profile::base::ntp_servers', {merge => 'unique'}),
  }

  # Configuring timezone
  class { 'timezone':
    timezone => lookup('profile::base::timezone'),
  }
}
