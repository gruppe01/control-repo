# Class for kubernetes role: node
class role::kubernetes::node {
  include ::profile::base
  include ::profile::kubernetes::base
  include ::profile::kubernetes::node
}
