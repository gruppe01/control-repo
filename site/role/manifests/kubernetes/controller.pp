# Class for kubernetes role: controller
class role::kubernetes::controller {
  include ::profile::base
  include ::profile::kubernetes::base
  include ::profile::kubernetes::etcd
  include ::profile::kubernetes::controller
}
