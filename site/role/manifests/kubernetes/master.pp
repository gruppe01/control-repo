# Class for kubernetes role: master
class role::kubernetes::master {
  include ::profile::base
  include ::profile::kubernetes::base
  include ::profile::kubernetes::etcd
  include ::profile::kubernetes::master
}
